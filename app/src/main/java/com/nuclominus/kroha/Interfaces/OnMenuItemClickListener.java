package com.nuclominus.kroha.Interfaces;

public interface OnMenuItemClickListener {
    void onClickMenuItem(int position);
}

