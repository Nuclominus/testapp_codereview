package com.nuclominus.kroha.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.content.ContextCompat;

import com.nuclominus.kroha.R;

public class ImageUtil {

    private static Bitmap bg;

    public static Bitmap getBg(Context context) {
        if (bg == null)
            bg = BitmapFactory.decodeResource(context.getResources(), R.drawable.bg_screen);
        return bg;
    }

    public static int getColor(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }
}
