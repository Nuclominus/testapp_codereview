package com.nuclominus.kroha.Utils;

import android.app.Application;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class AppInstance extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        PreferencesManager.initializeInstance(getApplicationContext());
//        FontsUtil.initializeInstance(getApplicationContext());
    }
}
