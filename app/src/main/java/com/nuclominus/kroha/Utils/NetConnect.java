package com.nuclominus.kroha.Utils;

import android.content.Context;
import android.util.Log;

import com.nuclominus.kroha.BuildConfig;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetConnect {

    public static NetConnect instance = null;
    private Context context;
    private Retrofit retrofit;

    public static NetConnect getInstance(Context context){
        if(instance == null){
            instance = new NetConnect();
        }
        instance.context = context;
        return instance;
    }

    private NetConnect(){
        try {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        } catch (IllegalArgumentException e){
            Log.e("Create retrofit error", e.getLocalizedMessage());
        }
    }

    public Retrofit getRetrofit(){
        return retrofit;
    }

}
