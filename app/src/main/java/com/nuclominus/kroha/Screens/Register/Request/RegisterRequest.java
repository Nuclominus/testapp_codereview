package com.nuclominus.kroha.Screens.Register.Request;

import com.nuclominus.kroha.Answers.AswToken;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RegisterRequest {

    @FormUrlEncoded
    @POST("/auth/registration")
    Call<AswToken> registerUser(@Field("login") String first,
                                @Field("password") String password,
                                @Field("firstname") String firstname,
                                @Field("lastname") String lastname,
                                @Field("email") String email);

}
