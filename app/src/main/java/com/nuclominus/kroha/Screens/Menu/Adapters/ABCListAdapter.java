package com.nuclominus.kroha.Screens.Menu.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Screens.Menu.Items.ABC_Item;

import java.util.ArrayList;


public class ABCListAdapter extends RecyclerView.Adapter<ABCListAdapter.ViewHolder> {

    private ArrayList<?> list;
    private Context context;

    public ABCListAdapter(ArrayList<?> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_abc_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ABC_Item item = (ABC_Item) list.get(position);
        holder.name.setText(item.getName());
    }

    @Override
    public int getItemCount() {
        if (list != null)
            return list.size();
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        protected Button name;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (Button) itemView.findViewById(R.id.btn_abc_name);
        }
    }

}
