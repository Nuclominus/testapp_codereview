package com.nuclominus.kroha.Screens.Menu.Fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nuclominus.kroha.Parents.ParentListFragment;
import com.nuclominus.kroha.R;

public class CategoryFragment extends ParentListFragment {

    public CategoryFragment() {
    }

    public static CategoryFragment newInstance() {
        CategoryFragment fragment = new CategoryFragment();
        return fragment;
    }

    @Override
    public void setAdapter() {
        super.setAdapter();


    }

    @Override
    public View getView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_category, container, false);
    }
}
