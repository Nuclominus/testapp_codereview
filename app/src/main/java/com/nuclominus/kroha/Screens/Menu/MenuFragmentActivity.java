package com.nuclominus.kroha.Screens.Menu;

import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.TextView;

import com.nuclominus.kroha.Interfaces.OnMenuItemClickListener;
import com.nuclominus.kroha.Parents.ParentFragment;
import com.nuclominus.kroha.Screens.Menu.Fragments.ABCListFragment;
import com.nuclominus.kroha.Screens.Menu.Fragments.BabyFragment;
import com.nuclominus.kroha.Screens.Menu.Logic.MenuFragmentLogic;
import com.nuclominus.kroha.Parents.ParentActivity;
import com.nuclominus.kroha.R;

public class MenuFragmentActivity extends ParentActivity implements OnMenuItemClickListener {

    enum SWITCH_ITEM{
        LIST,
        PICTURE
    }

    private ParentFragment f = null;
    FragmentManager fm = getSupportFragmentManager();

    private MenuFragmentLogic menuFragmentLogic;
    private TextView item_sex, item_switch;
    private SWITCH_ITEM switch_item;

    @Override
    protected void initUI() {
        super.initUI();
        setContentView(R.layout.activity_menu_fragment);

        setToolbarTitle(getString(R.string.abc_symptoms).replace("\n", ""), android.R.color.white);

        item_sex = (TextView) findViewById(R.id.tV_menuitem_sex);
        item_switch = (TextView) findViewById(R.id.tV_menuitem_switch);

        final View menuView = findViewById(R.id.includeToolbarBottom);
        menuFragmentLogic = new MenuFragmentLogic(this, menuView, this, bundle);
        switch_item = SWITCH_ITEM.PICTURE;
        onClickMenuItem(2);

    }

    @Override
    public void onClickMenuItem(int position) {

        switch (position) {
            case 1: {
                if(f != null){
                    if(bundle.getInt(getString(R.string.sex)) == 1){
                        bundle.putInt(getString(R.string.sex), 2);
                    } else {
                        bundle.putInt(getString(R.string.sex), 1);
                    }
                    f.updateUI(bundle);
                }
            }
            break;

            case 2: {
                if(switch_item == SWITCH_ITEM.PICTURE){
                    f = BabyFragment.newInstance();
                    f.setArguments(bundle);
                    switch_item = SWITCH_ITEM.LIST;
                } else if(switch_item == SWITCH_ITEM.LIST){
                    f = ABCListFragment.newInstance();
                    f.setArguments(bundle);
                    switch_item = SWITCH_ITEM.PICTURE;
                }
            }
            break;
        }

        if (f != null)
            fm.beginTransaction()
                    .replace(R.id.contentFrame, f)
                    .addToBackStack(null)
                    .commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
