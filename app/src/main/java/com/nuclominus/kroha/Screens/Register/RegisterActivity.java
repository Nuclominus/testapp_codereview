package com.nuclominus.kroha.Screens.Register;

import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.nuclominus.kroha.Answers.AswToken;
import com.nuclominus.kroha.Parents.ParentActivity;
import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Screens.MainMenu.MainMenuActivity;
import com.nuclominus.kroha.Screens.Register.Request.RegisterRequest;
import com.nuclominus.kroha.Utils.NetConnect;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends ParentActivity implements View.OnClickListener{

    private EditText tv_register_name,
            tv_register_surname,
            tv_register_phone,
            tv_register_email,
            tv_register_password;
    private CheckBox offer;
    private NetConnect netConnect;

    @Override
    protected void initUI() {
        super.initUI();
        setContentView(R.layout.activity_register);
        setToolbarTitle(getString(R.string.sign_up), android.R.color.white);

        netConnect = NetConnect.getInstance(this);

        offer = (CheckBox) findViewById(R.id.cB_offer);

        tv_register_name = (EditText) findViewById(R.id.tv_register_name);
        tv_register_surname = (EditText) findViewById(R.id.tv_register_surname);
        tv_register_phone = (EditText) findViewById(R.id.tv_register_phone);
        tv_register_password = (EditText) findViewById(R.id.tv_register_password);
        tv_register_email = (EditText) findViewById(R.id.tv_register_email);

        Button mEmailSignUpButton = (Button) findViewById(R.id.btn_sign_up);
        mEmailSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegister();
            }
        });
    }

    private void attemptRegister() {

        tv_register_phone.setError(null);
        tv_register_password.setError(null);

        String firstname = tv_register_name.getText().toString();
        String surname = tv_register_surname.getText().toString();
        String phone = tv_register_phone.getText().toString();
        String email = tv_register_email.getText().toString();
        String password = tv_register_password.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            tv_register_password.setError(getString(R.string.error_invalid_password));
            focusView = tv_register_password;
            cancel = true;
        }

        if (TextUtils.isEmpty(phone)) {
            tv_register_phone.setError(getString(R.string.error_field_required));
            focusView = tv_register_phone;
            cancel = true;
        } else if (!isPhoneValid(phone)) {
            tv_register_phone.setError(getString(R.string.error_invalid_phone));
            focusView = tv_register_phone;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            RegisterRequest registerRequest = netConnect.getRetrofit().create(RegisterRequest.class);
            Call<AswToken> response = registerRequest.registerUser(
                    firstname,
                    surname,
                    phone,
                    email,
                    password);

            response.enqueue(new Callback<AswToken>() {
                @Override
                public void onResponse(Call<AswToken> call, Response<AswToken> response) {
                    AswToken token = response.body();
                    token.saveToken();
                    startActivity(new Intent(RegisterActivity.this, MainMenuActivity.class));
                    finish();
                }

                @Override
                public void onFailure(Call<AswToken> call, Throwable t) {
                    Log.d("ERROR", t.getLocalizedMessage());
                }
            });
        }
    }

    private boolean isPhoneValid(String phone) {
        return phone.length() == 12 ? true : false;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sign_up: {
                if (offer.isChecked()) {
                    attemptRegister();
                }
            }
            break;
        }
    }
}
