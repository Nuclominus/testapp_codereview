package com.nuclominus.kroha.Screens.Menu.Logic;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nuclominus.kroha.Interfaces.OnMenuItemClickListener;
import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Utils.ImageUtil;

public class MenuFragmentLogic implements View.OnClickListener {

    public enum BabyChoose{
        BOY(1),
        GIRL(2),
        PICTURE(3),
        LIST(4);

        private int value;
        BabyChoose(int value){
            this.value = value;
        }
    }

    private RelativeLayout layout_action_1, layout_action_2;
    private ImageView iV_action_1, iV_action_2;
    private TextView tV_sex, tV_list;
    private Context context;
    private OnMenuItemClickListener listener;
    private BabyChoose chooseSex, chooseList;

    public MenuFragmentLogic(Context context, View menu, OnMenuItemClickListener listener, Bundle bundle) {
        layout_action_1 = (RelativeLayout) menu.findViewById(R.id.ll_action_baby_book);
        layout_action_2 = (RelativeLayout) menu.findViewById(R.id.ll_action_ABC);

        iV_action_1 = (ImageView) menu.findViewById(R.id.iV_action_baby_book);
        iV_action_2 = (ImageView) menu.findViewById(R.id.iV_action_ABC);

        tV_sex = (TextView) menu.findViewById(R.id.tV_menuitem_sex);
        tV_list = (TextView) menu.findViewById(R.id.tV_menuitem_switch);

        if (listener != null)
            this.listener = listener;

        layout_action_1.setOnClickListener(this);
        layout_action_2.setOnClickListener(this);

        this.context = context;

        chooseList = BabyChoose.LIST;

        if (bundle != null) {
            Integer active = bundle.getInt(context.getString(R.string.sex));
            if (active != null) {
                if (active == 2) {
                    chooseSex = BabyChoose.BOY;
                } else if (active == 1) {
                    chooseSex = BabyChoose.GIRL;
                }
            }
        }
        
        updateSex();
        updateList();
    }

//    protected void switchActive(int active) {
//        switch (active) {
//            case 1: {
//                onClick(layout_action_1);
//            }
//            break;
//
//            case 2: {
//                onClick(layout_action_2);
//            }
//            break;
//        }
//    }

    @Override
    public void onClick(View v) {
        int action = 0;
        switch (v.getId()) {
            case R.id.ll_action_baby_book: {
                action = 1;
                setAction(action);
            }
            break;

            case R.id.ll_action_ABC: {
                action = 2;
                setAction(action);
            }
            break;
        }

        if (listener != null) {
            listener.onClickMenuItem(action);
        }
    }

    public void setAction(int action) {
        switch (action) {
            case 1: {
                updateSex();
            }
            break;

            case 2: {
                updateList();
            }
            break;
        }
    }

    private void updateSex(){
        if (chooseSex == BabyChoose.BOY) {
            iV_action_1.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_boy));
            tV_sex.setText(context.getString(R.string.boy));
            chooseSex = BabyChoose.GIRL;
        } else if (chooseSex == BabyChoose.GIRL) {
            iV_action_1.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_girl));
            tV_sex.setText(context.getString(R.string.girl));
            chooseSex = BabyChoose.BOY;
        }
    }

    private void updateList(){
        if(chooseList == BabyChoose.LIST) {
//                    iV_action_2.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_3_1));
            tV_list.setText(context.getString(R.string.list));
            chooseList = BabyChoose.PICTURE;
        } else {
//                    iV_action_2.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_3_1));
            tV_list.setText(context.getString(R.string.picture));
            chooseList = BabyChoose.LIST;
        }
    }
}
