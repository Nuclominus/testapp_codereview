package com.nuclominus.kroha.Screens.Menu.Fragments;

import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nuclominus.kroha.Parents.ParentListFragment;
import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Screens.Menu.Adapters.ABCListAdapter;
import com.nuclominus.kroha.Screens.Menu.Items.ABC_Item;

import java.util.ArrayList;

public class ABCListFragment extends ParentListFragment {

    private ArrayList<ABC_Item> listArray;
    private ABCListAdapter adapter;

    public static ABCListFragment newInstance() {
        ABCListFragment fragment = new ABCListFragment();
        return fragment;
    }

    public void setAdapter() {
        String[] mArray = getResources().getStringArray(R.array.organs);
        if (listArray == null)
            listArray = new ArrayList<>();

        setData(listArray, mArray);

        adapter = new ABCListAdapter(listArray, getActivity());
        recyclerView.setAdapter(adapter);
    }

    @Nullable
    @Override
    public View getView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_abclist, container, false);
    }
}
