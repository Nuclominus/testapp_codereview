package com.nuclominus.kroha.Screens.Login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nuclominus.kroha.Answers.AswToken;
import com.nuclominus.kroha.Parents.ParentActivity;
import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Screens.Login.Logic.LoginLogic;
import com.nuclominus.kroha.Screens.Login.Request.LoginRequest;
import com.nuclominus.kroha.Screens.MainMenu.MainMenuActivity;
import com.nuclominus.kroha.Screens.Register.RegisterActivity;
import com.nuclominus.kroha.Utils.NetConnect;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends ParentActivity implements LoginLogic.LoginProgress {

    private AutoCompleteTextView mPhoneView;
    private EditText mPasswordView;
    private LoginLogic loginLogic;

    @Override
    protected void initUI() {
        super.initUI();
        setContentView(R.layout.activity_login);

        mPhoneView = (AutoCompleteTextView) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);

        loginLogic = new LoginLogic(this,
                this,
                findViewById(R.id.login_form),
                findViewById(R.id.login_progress));

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    loginLogic.attemptLogin(mPhoneView, mPasswordView);
                    return true;
                }
                return false;
            }
        });


        Button mSignInButton = (Button) findViewById(R.id.btn_sign_in);
        mSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                loginLogic.attemptLogin(mPhoneView, mPasswordView);
            }
        });

        Button register = (Button) findViewById(R.id.btn_register);
        register.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });
    }

    @Override
    public void successLogin() {
        startActivity(new Intent(LoginActivity.this, MainMenuActivity.class));
        finish();
    }

    @Override
    public void failLogin() {

    }
}

