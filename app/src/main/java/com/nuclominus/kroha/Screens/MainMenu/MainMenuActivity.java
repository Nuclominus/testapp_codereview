package com.nuclominus.kroha.Screens.MainMenu;

import android.content.Intent;
import android.view.View;

import com.nuclominus.kroha.Interfaces.OnMenuItemClickListener;
import com.nuclominus.kroha.Screens.ChoseBaby.ChoseBabyActivity;
import com.nuclominus.kroha.Screens.MainMenu.Logic.MainMenuItemLogic;
import com.nuclominus.kroha.Parents.ParentActivity;
import com.nuclominus.kroha.R;

public class MainMenuActivity extends ParentActivity implements OnMenuItemClickListener{

    private MainMenuItemLogic mainMenuItemLogic;

    @Override
    protected void initUI() {
        super.initUI();
        setContentView(R.layout.activity_main_menu);

        setToolbarTitle(getString(R.string.app_name), R.color.colorTitleYellow);

        final View menuView = findViewById(R.id.ll_category_menu);
        mainMenuItemLogic = new MainMenuItemLogic(this,menuView,this);
    }

    @Override
    public void onClickMenuItem(int position) {
        switch (position){
            case 1:{
//                startActivity(new Intent(this, ChoseBabyActivity.class));
            }break;

            case 2:{
                startActivity(new Intent(this, ChoseBabyActivity.class));
            }break;

            case 3:{

            }break;
        }
    }
}
