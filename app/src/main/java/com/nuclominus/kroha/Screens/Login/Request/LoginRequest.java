package com.nuclominus.kroha.Screens.Login.Request;

import com.nuclominus.kroha.Answers.AswToken;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginRequest {

    @FormUrlEncoded
    @POST("/auth/login")
    Call<AswToken> loginUser(@Field("login") String login, @Field("password") String password);

}
