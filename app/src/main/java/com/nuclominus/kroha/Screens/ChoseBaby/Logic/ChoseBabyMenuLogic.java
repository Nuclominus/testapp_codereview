package com.nuclominus.kroha.Screens.ChoseBaby.Logic;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.nuclominus.kroha.Interfaces.OnMenuItemClickListener;
import com.nuclominus.kroha.R;

import me.grantland.widget.AutofitTextView;


public class ChoseBabyMenuLogic implements View.OnClickListener {

    private OnMenuItemClickListener listener;
    private View btn1, btn2;
    private Context context;

    public ChoseBabyMenuLogic(Context context, View menuView, OnMenuItemClickListener listener) {
        btn1 = menuView.findViewById(R.id.iBtn_sex_1);
        btn2 = menuView.findViewById(R.id.iBtn_sex_2);

        this.context = context;
        this.listener = listener;

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);

        initChoseBabyMenuUI();
    }

    private void initChoseBabyMenuUI(){
        ((ImageView)btn1.findViewById(R.id.iV_btn_category_icon)).setImageResource(R.drawable.icon_boy);
        ((ImageView)btn2.findViewById(R.id.iV_btn_category_icon)).setImageResource(R.drawable.icon_girl);

        ((AutofitTextView)btn1.findViewById(R.id.btn_category_title)).setText(context.getString(R.string.boy));
        ((AutofitTextView)btn2.findViewById(R.id.btn_category_title)).setText(context.getString(R.string.girl));
    }


    @Override
    public void onClick(View v) {
        if (v == btn1) {
            listener.onClickMenuItem(1);
        } else if (v == btn2) {
            listener.onClickMenuItem(2);
        }
    }
}
