package com.nuclominus.kroha.Screens.ChoseBaby;

import android.content.Intent;
import android.view.View;

import com.nuclominus.kroha.Interfaces.OnMenuItemClickListener;
import com.nuclominus.kroha.Parents.ParentActivity;
import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Screens.ChoseBaby.Logic.ChoseBabyMenuLogic;
import com.nuclominus.kroha.Screens.Menu.MenuFragmentActivity;

public class ChoseBabyActivity extends ParentActivity implements OnMenuItemClickListener{

    private ChoseBabyMenuLogic menuLogic;

    @Override
    protected void initUI() {
        super.initUI();
        setContentView(R.layout.activity_chose_baby);

        setToolbarTitle(getString(R.string.baby), android.R.color.white);

        final View menuView = findViewById(R.id.ll_baby_category_menu);
        menuLogic = new ChoseBabyMenuLogic(this,menuView,this);
    }

    @Override
    public void onClickMenuItem(int position) {
        Intent intent = new Intent(this, MenuFragmentActivity.class);
        switch (position){
            case 1:{
                intent.putExtra(getString(R.string.sex),1);
            }break;

            case 2:{
                intent.putExtra(getString(R.string.sex),2);
            }break;
        }
        intent.putExtra(getString(R.string.bottom_menu_active),2);
        startActivity(intent);
    }
}
