package com.nuclominus.kroha.Parents;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Utils.ImageUtil;

import me.grantland.widget.AutofitHelper;
import me.grantland.widget.AutofitTextView;

public class ParentActivity extends AppCompatActivity {

    public Toolbar toolbar;
    public Bundle bundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getIntent().getExtras();
        initUI();
//        setStatusBarTranslucent(true);
        loadBG();

        changeFont(getWindow().getDecorView().getRootView());
        if (findViewById(R.id.toolbar) != null) {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        showLeftAnimation(this);
    }

    protected void initUI() {
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    public void setToolbarTitle(String title, int color) {
        AutofitHelper.create(((TextView) findViewById(R.id.main_toolbar_title)));
        ((AutofitTextView) findViewById(R.id.main_toolbar_title)).setText(title);
        ((AutofitTextView) findViewById(R.id.main_toolbar_title)).setTextColor(ImageUtil.getColor(this, color));
        ((AutofitTextView) findViewById(R.id.main_toolbar_title)).setTextSize(22);
//        ((AutofitTextView) findViewById(R.id.main_toolbar_title)).setTypeface(FontsUtil.getInstance().getLatoBold());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean result;

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                result = true;
                break;
            default:
                result = super.onOptionsItemSelected(item);
        }
        return result;
    }

    protected void changeFont(View v) {
//        FontsUtil.overrideFonts(this, v);
    }

    protected void loadBG() {
        if (findViewById(R.id.iv_bg) != null)
            ((ImageView) findViewById(R.id.iv_bg)).setImageBitmap(ImageUtil.getBg(this));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        showRightAnimation(this);
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        showRightAnimation(this);
    }

    public static void showLeftAnimation(final Activity activity) {
        activity.overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left);
    }

    public static void showRightAnimation(final Activity activity) {
        activity.overridePendingTransition(R.anim.anim_slide_in_right, R.anim.anim_slide_out_right);
    }

}

