package com.nuclominus.kroha.Parents;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nuclominus.kroha.R;
import com.nuclominus.kroha.Screens.Menu.Adapters.ABCListAdapter;
import com.nuclominus.kroha.Screens.Menu.Items.ABC_Item;
import com.nuclominus.kroha.Utils.RecyclerItemClickListener;

import java.util.ArrayList;

public abstract class ParentListFragment extends ParentFragment {

    public RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    public ProgressBar load;
    public SwipeRefreshLayout refreshView;

    public ParentListFragment() {

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.rV_list);
        load = (ProgressBar) rootView.findViewById(R.id.pB_load);
        refreshView = (SwipeRefreshLayout) rootView.findViewById(R.id.refreshView);

        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(false);
        recyclerView.setAdapter(null);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                refreshView.setEnabled(topRowVerticalPosition >= 0);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        refreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshView.setRefreshing(false);
            }
        });

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                    }
                })
        );
        setAdapter();
    }

    public void setAdapter() {};

    public  <T> void setData(ArrayList<T> list, String[] data ){
        for (int i = 0; i < data.length; i++) {
            Object item = new ABC_Item(i + 1, data[i]);
            list.add((T) item);
        }
    }

}
