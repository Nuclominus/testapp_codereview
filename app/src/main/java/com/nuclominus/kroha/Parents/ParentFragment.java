package com.nuclominus.kroha.Parents;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ParentFragment extends Fragment{

    protected View rootView;
    protected Bundle bundle;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        bundle = getArguments();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = getView(inflater, container);
        return rootView;
    }

    public void updateUI(Bundle bundle){}

    public View getView(LayoutInflater inflater, ViewGroup container){
        return null;
    }
}
